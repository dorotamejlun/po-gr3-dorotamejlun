package pl.edu.uwm.wmii.mejlundorota.Kolokwium;


public class Kandydat implements Cloneable, Comparable {
    private String nazwa;
    private int wiek;
    private String wyksztalcony;
    private int latadoswiadczenia;

    public Kandydat(String nazwa, int wiek, String wyksztalcony, int latadoswiadczenia) {
        this.nazwa = nazwa;
        this.wiek = wiek;
        this.wyksztalcony = wyksztalcony;
        this.latadoswiadczenia = latadoswiadczenia;
    }

    public String getNazwa() {
        return nazwa;
    }

    public int getWiek() {
        return wiek;
    }

    public String getWyksztalcony() {
        return wyksztalcony;
    }

    public int getLatadoswiadczenia() {
        return latadoswiadczenia;
    }

    @Override
    public int compareTo(Object o) {
        Kandydat obiekt = (Kandydat) o;
        int i = wyksztalcony.compareTo(obiekt.wyksztalcony);
        if (i != 0) return i;
        int t = new Integer(wiek).compareTo(obiekt.wiek);
        if (t != 0) return t;
        return new Integer(latadoswiadczenia).compareTo(obiekt.latadoswiadczenia);

    }

    @Override
    public String toString() {
        return "Kandydat {" +
                "nazwa ='" + nazwa + '\'' +
                ", wiek =" + wiek +
                ", wyksztalcony ='" + wyksztalcony + '\'' +
                ", latadoswiadczenia =" + latadoswiadczenia +
                " }";
    }
}