package pl.edu.uwm.wmii.mejlundorota.Kolokwium;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Collection;
import java.util.Map;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        ArrayList<Kandydat> grupa = new ArrayList<>();
        grupa.add(new Kandydat("Kowalski", 30, "licencjat", 0));
        grupa.add(new Kandydat("Nowak", 30, "mistrzowie", 4));
        grupa.add(new Kandydat("Mazur", 40, "licencjat", 3));
        grupa.add(new Kandydat("Wloch", 45, "licencjat", 3));
        grupa.add(new Kandydat("Zbik", 50, "mistrzowie", 2));
        grupa.add(new Kandydat("Alba", 50, "mistrzowie", 15));

        System.out.println(grupa.toString());
        Collections.sort(grupa);
        for (int i = 0; i < grupa.size(); i++) {
            System.out.print("Index: " + i + ": ");
            System.out.println(Rekrutacja.Qualified(grupa.get(i)));
        }
        System.out.println(RecruitmentMap(grupa));
    }

    public static Map RecruitmentMap(ArrayList<Kandydat> klist){
        Map<Integer, String> mapOfAnything = new HashMap<>();
        for(Kandydat k: klist){
            if(Rekrutacja.Qualified(k)){
                mapOfAnything.put(new Integer(k.getLatadoswiadczenia()), k.getNazwa());
            }
        }
        return mapOfAnything;
    }
}