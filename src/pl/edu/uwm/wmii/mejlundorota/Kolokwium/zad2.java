package pl.edu.uwm.wmii.mejlundorota.Kolokwium;

import java.util.ArrayList;

public class zad2 {
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> c = new ArrayList<>();
        if(a.size() < b.size()){
            for (int counter = 0; counter < b.size(); counter++) {
                boolean was = false;
                for (int i = 0; i<c.size(); i++) {
                    if (b.get(counter) == c.get(i)) {
                        was = true;
                        break;
                    }
                }
                if(!was){
                    c.add(b.get(counter));
                }
                if(counter < a.size()){
                    boolean was2 = false;
                    for (int i = 0; i<c.size(); i++) {
                        if (a.get(counter) == c.get(i)) {
                            was2 = true;
                            break;
                        }
                    }
                    if(!was2){
                        c.add(a.get(counter));
                    }
                }
            }
        } else {
            for (int counter = 0; counter < a.size(); counter++) {
                boolean was = false;
                for (int i = 0; i<c.size(); i++) {
                    if (a.get(counter) == c.get(i)) {
                        was = true;
                        break;
                    }
                }
                if(!was){
                    c.add(a.get(counter));
                }
                if(counter < b.size()){
                    boolean was2 = false;
                    for (int i = 0; i<c.size(); i++) {
                        if (b.get(counter) == c.get(i)) {
                            was = true;
                            break;
                        }
                    }
                    if(!was2){
                        c.add(b.get(counter));
                    }
                }
            }
        }
        return c;
    }

    public static void main(String[] args) {
        int[] ids = {1, 2, 3, 4, 5, 6};
        int[] ids2 = {3, 8, 5};

        ArrayList<Integer> a = new ArrayList<>();
        ArrayList<Integer> b = new ArrayList<>();

        for (int id: ids) {
            a.add(id);
        }
        for(int id: ids2) {
            b.add(id);
        }

        System.out.println(a);
        System.out.println(b);
        ArrayList<Integer> c = merge(a, b);
        System.out.println(c);
    }

}