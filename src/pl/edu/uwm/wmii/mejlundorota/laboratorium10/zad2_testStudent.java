package pl.edu.uwm.wmii.mejlundorota.laboratorium10;

import pl.imiajd.mejlun.Student3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class zad2_testStudent {
    public static void main(String[] args) {
        ArrayList<Student3> grupa = new ArrayList<>();
        grupa.add(new Student3("Jan Kowalski", 1980, 8, 18, 4.0 ));
        grupa.add(new Student3("Jan Kowalski", 1990, 9, 19, 5.0));
        grupa.add(new Student3("Jakub Mazur", 1970, 7, 17, 4.5));
        grupa.add(new Student3("Piotr Nowak", 1970, 7, 17, 4.5));
        grupa.add(new Student3("Dorota Mejłun", 1999, 3, 15, 3.0));
        Iterator var = grupa.iterator();

        Student3 p;
        while(var.hasNext()) {
            p = (Student3)var.next();
            System.out.println(p.toString());
        }

        Collections.sort(grupa);
        System.out.println("Po posortowaniu: ");
        var = grupa.iterator();

        while(var.hasNext()) {
            p = (Student3)var.next();
            System.out.println(p.toString());
        }
    }
}
