package pl.edu.uwm.wmii.mejlundorota.laboratorium10;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import pl.imiajd.mejlun.*;

public class zad1_testOsoba {
    public static void main(String[] args) {
        ArrayList<Osoba3> grupa = new ArrayList<>();
        grupa.add(new Osoba3("Jan Kowalski", 1980, 8, 18));
        grupa.add(new Osoba3("Jan Kowalski", 1990, 9, 19));
        grupa.add(new Osoba3("Jakub Mazur", 1970, 7, 17));
        grupa.add(new Osoba3("Piotr Nowak", 1970, 7, 17));
        grupa.add(new Osoba3("Dorota Mejłun", 1999, 3, 15));
        Iterator var = grupa.iterator();

        Osoba3 p;
        while(var.hasNext()) {
            p = (Osoba3)var.next();
            System.out.println(p.toString());
        }

        Collections.sort(grupa);
        System.out.println("Po posortowaniu: ");
        var = grupa.iterator();

        while(var.hasNext()) {
            p = (Osoba3)var.next();
            System.out.println(p.toString());
        }
    }

}