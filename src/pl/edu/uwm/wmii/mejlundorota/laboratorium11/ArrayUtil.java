package pl.edu.uwm.wmii.mejlundorota.laboratorium11;

import java.util.ArrayList;

public class ArrayUtil {
    public static <T extends Comparable <? super T>> boolean isSorted(ArrayList<T> tablica){
        for (int i= 0; i<tablica.size()-1; i++){
            if(tablica.get(i).compareTo(tablica.get(i+1)) > 0){
                return false;
            }
        }
        return true;
    }
    public static  <T extends Comparable <? super T>>  int binSearch (ArrayList<T> tablica, T obiekt){
        for (int i=0; i<tablica.size(); i++){
            if(tablica.get(i).equals(obiekt)){
                return i;
            }
        }
        return -1;
    }
    public static <T extends Comparable <? super T>> void selectionSort (ArrayList<T> tablica){
        for (int i=0;i<tablica.size()-1;i++){
            int min = i;
            for (int j=i+1;j<tablica.size();j++) {
                if (tablica.get(min).compareTo(tablica.get(j)) > 0) {
                    min = j;
                }
            }
            T tmp = tablica.get(i);
            tablica.set(i, tablica.get(min));
            tablica.set(min, tmp);
        }
    }
}

