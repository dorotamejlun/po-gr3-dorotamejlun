package pl.edu.uwm.wmii.mejlundorota.laboratorium11;
import java.time.LocalDate;
import java.util.ArrayList;

public class test_ArrayUtil {

    public static void main(String[]args){

        int [] l1 = {1,2,3,4,5};
        int [] l2 = {15,14,13,12,11};
        int [] l3 = {4,5,3,1,2};

        ArrayList<Integer> a1 = new ArrayList<>();
        ArrayList<Integer> a2 = new ArrayList<>();
        ArrayList<Integer> a3 = new ArrayList<>();

        ArrayList<LocalDate> d1 = new ArrayList<>();
        ArrayList<LocalDate> d2 = new ArrayList<>();

        d1.add(LocalDate.of(1980, 8,18));
        d1.add(LocalDate.of(1990, 9,19));
        d1.add(LocalDate.of(1970, 7,17));

        d2.add(LocalDate.of(1970, 7,17));
        d2.add(LocalDate.of(1980, 8,18));
        d2.add(LocalDate.of(1990, 9,19));

        for (int e: l1 ){
            a1.add(e);
        }
        for (int e: l2 ){
            a2.add(e);
        }
        for (int e: l3 ){
            a3.add(e);
        }

        System.out.println(a1 + " a1: " + ArrayUtil.isSorted(a1));
        System.out.println(a2 + " a2: " + ArrayUtil.isSorted(a2));
        System.out.println(a3 + " a3: " + ArrayUtil.isSorted(a3));

        System.out.println(d1 + " d1: " + ArrayUtil.isSorted(d1));
        System.out.println(d2 + " d2: " + ArrayUtil.isSorted(d2));

        System.out.println("Metoda binSearch: " + ArrayUtil.binSearch(a1,2));
        System.out.println("Metoda binSearch: " + ArrayUtil.binSearch(a2,3));

        ArrayUtil.selectionSort(a2);

        System.out.println(a2);

    }

}
