package pl.edu.uwm.wmii.mejlundorota.laboratorium11;

public class PairUtil {
    public static <T> Pair<T> swap(Pair<T> para ){
        return new Pair<T>(para.getSecond(), para.getFirst());
    }
}
