package pl.edu.uwm.wmii.mejlundorota.laboratorium06;

public class zad2 {
    public static void main(String[] args) throws Exception {
        IntegerSet i1 = new IntegerSet();
        System.out.println(i1.toString());

        i1.insertElement(2);
        i1.insertElement(7);


        i1.deleteElement(2);
        System.out.println(i1.toString());

        IntegerSet i2 = new IntegerSet();

        i2.insertElement(9);
        i2.insertElement(7);
        i2.insertElement(10);

        System.out.println(i2.toString());

        IntegerSet i3 = IntegerSet.union(i1, i2);
        System.out.println(i3.toString());

        IntegerSet i4 = IntegerSet.intersection(i1, i2);
        System.out.println(i4.toString());

        System.out.println(i4.equals(i2));

    }
}
class RachunekBankowy {
    static double rocznaStopaProcentowa;
    private double saldo;

    public RachunekBankowy(double saldo){
        this.saldo = saldo;
    }

    public void obliczMiesieczneOdsetki(){
        this.saldo += (this.saldo * rocznaStopaProcentowa) / 12;
    }

    public static void setRocznaStopaProcentowa(double stopa){
        rocznaStopaProcentowa = stopa;
    }

    public double getSaldo(){
        return this.saldo;
    }
}