package pl.edu.uwm.wmii.mejlundorota.laboratorium06;

public class zad1 {
    public static void main(String[] args) {
        RachunekBankowy saver1 = new RachunekBankowy(2000);
        RachunekBankowy saver2 = new RachunekBankowy(3000);
        RachunekBankowy.setRocznaStopaProcentowa(0.04);

        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println(saver1.getSaldo());
        System.out.println(saver2.getSaldo());

        RachunekBankowy.setRocznaStopaProcentowa(0.05);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println(saver1.getSaldo());
        System.out.println(saver2.getSaldo());
    }
}
class IntegerSet {
    private final Boolean[] zbior;

    public IntegerSet(){
        this.zbior = new Boolean[100];
        for(int i = 0; i<100; i++){
            this.zbior[i] = false;
        }
    }

    public void insertElement(int liczba) throws Exception {
        if(liczba < 1 || liczba > 100){
            throw new Exception("Zła liczba");
        }
        this.zbior[liczba-1] = true;
    }

    public Boolean getElement(int i){
        return this.zbior[i];
    }

    public static IntegerSet union(IntegerSet i1, IntegerSet i2) throws Exception {
        IntegerSet i3 = new IntegerSet();
        for(int i = 0; i<100; i++){
            if(i1.getElement(i) || i2.getElement(i)){
                i3.insertElement(i+1);
            }
        }
        return i3;
    }

    public static IntegerSet intersection(IntegerSet i1, IntegerSet i2) throws Exception {
        IntegerSet i3 = new IntegerSet();
        for(int i = 0; i<100; i++){
            if(i1.getElement(i) && i2.getElement(i)){
                i3.insertElement(i+1);
            }
        }
        return i3;
    }

    public void deleteElement(int liczba) throws Exception {
        if(liczba < 1 || liczba > 100){
            throw new Exception("Zła liczba");
        }
        this.zbior[liczba-1] = false;
    }

    @Override
    public String toString() {
        StringBuilder napis = new StringBuilder(" ");
        for(int i = 0; i<100; i++){
            if(this.zbior[i]){
                napis.append(i + 1).append(",");
            }
        }
        return napis.toString();
    }


    public Boolean equals(IntegerSet i2){
        for(int i = 0; i<100; i++){
            if(this.zbior[i] != i2.getElement(i)){
                return false;
            }
        }
        return true;
    }
}
