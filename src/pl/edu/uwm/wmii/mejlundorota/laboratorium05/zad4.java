package pl.edu.uwm.wmii.mejlundorota.laboratorium05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;

public class zad4 {
    public static ArrayList<Integer> reversed(ArrayList<Integer> a){
        ArrayList<Integer> c = new ArrayList<>();

        for (int counter = a.size()-1; counter >= 0; counter--) {
            c.add(a.get(counter));
        }
        return c;
    }

    public static void main(String[] args) {
        int[] ids = {4, 7, 9, 9, 11};

        ArrayList<Integer> a = new ArrayList<>();

        for (int id: ids) {
            a.add(id);
        }
        System.out.println(a);
        ArrayList<Integer> c = reversed(a);
        System.out.println(c);
    }
}