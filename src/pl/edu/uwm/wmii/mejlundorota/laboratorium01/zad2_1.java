package pl.edu.uwm.wmii.mejlundorota.laboratorium01;
import java.util.Scanner;

public class zad2_1 {
    public static void main(String[] args) {
        int n;
        Scanner in = new Scanner(System.in);

        System.out.println("Enter a number: ");
        n = in.nextInt();
        System.out.println("Enter " + n + " numbers: ");

        int[] tab = new int[n];
        for (int i = 1; i <= n; i++) {
            tab[i-1] = in.nextInt();
        }
        _2a(tab, n);
        _2b(tab, n);
        _2c(tab, n);
        _2d(tab, n);
        _2e(tab, n);
        _2f(tab, n);
        _2g(tab, n);
        _2h(tab, n);
    }
    public static void _2a(int[] tab, int n) {
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (tab[i] % 2 != 0) {
                count++;
            }
        }
        System.out.println("2a) The num of odd nums: " + count);
    }
    public static void _2b(int[] tab, int n) {
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (tab[i] % 3 == 0 && tab[i] % 5 != 0) {
                count++;
            }
        }
        System.out.println("2b) The num of nums %3==0 and %5!=0 : " + count);
    }
    public static void _2c(int[] tab, int n) {
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (tab[i] % 2 == 0 && Math.sqrt(tab[i]) == (int) Math.sqrt(tab[i])) {
                count++;
            }
        }
        System.out.println("2c) The num of nums that are sqr of an even num : " + count);
    }
    public static void _2d(int[] tab, int n) {
        int count = 0;
        for (int i = 1; i < n - 1; i++) {
            if (tab[i] < (tab[i - 1] + tab[i + 1]) * 0.5) {
                count++;
            }
        }
        System.out.println("2d) The num of nums that are under the condition a_k<(a_k-1 + a_k+1)/ 2: " + count);
    }
    private static int factorial(int m) {
        if (m < 1)
            return 1;
        else
            return m * factorial(m - 1);
    }
    public static void _2e(int[] tab, int n) {

        int count = 0;
        for (int i = 1; i < n; i++) {
            if (tab[i] > ((int) Math.pow(2, i)) && tab[i] < factorial(tab[i])) {
                count++;
            }
        }
        System.out.println("2e) The num of nums that are under the condition 2^k<a_k<k!: " + count);
    }
    public static void _2f(int[] tab, int n) {
        int count = 0;
        for (int i=0; i<n; i++){
            if ((i%2 == 0) && (tab[i]%2 == 0)) {
                count++;
            }
        }
        System.out.println("2f) The num of even nums at odd pos: " + count);
    }
    public static void _2g(int[] tab, int n) {
        int count = 0;
        for (int i=0; i<n; i++){
            if ((tab[i]%2!=0) && (tab[i]>=0) ) {
                count++;
            }
        }
        System.out.println("2g) The num of odd and non-negative nums: " + count);
    }
    public static void _2h(int[] tab, int n) {
        int count = 0;
        for (int i=0; i<n; i++){
            if (Math.abs(tab[i]) < Math.pow(i,2)) {
                count++;
            }
        }
        System.out.println("2h) The num of nums that are under the condition |a_k|<k^2: " + count);
    }
}
