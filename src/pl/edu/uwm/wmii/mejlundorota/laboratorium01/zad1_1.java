package pl.edu.uwm.wmii.mejlundorota.laboratorium01;
import java.lang.*;

public class zad1_1 {

    public static void main(String[] args) {
        //ex1.a-g
        int lista1[] = {-1,3,-5,-16};
        int lista2[] = {16,4,25,16};
        int suma = 0;
        int iloczyn = 1;
        int suma_wart_bezw = 0;
        int suma_pierw = 0;
        int iloczyn_wart_bezw = 1;
        int potega_suma = 0;
        int suma_naprzemian = 0;
        for (int i=0; i<4; i++){
            suma += lista1[i];
            iloczyn *= lista1[i];
            if (lista1[i]>0){
                suma_wart_bezw += lista1[i];
            }
            else{
                suma_wart_bezw += (-1)*lista1[i];
            }
            suma_pierw += Math.sqrt(lista2[i]);
            if (lista1[i]>0){
                iloczyn_wart_bezw *= lista1[i];
            }
            else{
                iloczyn_wart_bezw *= (-1)*lista1[i];
            }
            potega_suma += lista1[i]*lista1[i];
            if (i%2==1){
                suma_naprzemian += lista1[i]*(-1);
            }
            else{
                suma_naprzemian += lista1[i];
            }
        }
        System.out.println("Suma: "+suma);
        System.out.println("Iloczyn: "+iloczyn);
        System.out.println("|Suma|: "+suma_wart_bezw);
        System.out.println("Suma pierw.: "+suma_pierw);
        System.out.println("Iloczyn z wart. bezw.: "+iloczyn_wart_bezw);
        System.out.println("Suma potęg: "+ potega_suma);
        System.out.println("Suma naprzemian: "+ suma_naprzemian);
    }
}