package pl.edu.uwm.wmii.mejlundorota.laboratorium07;
import pl.imiajd.mejlun.Osoba;
import pl.imiajd.mejlun.Student;
import pl.imiajd.mejlun.Nauczyciel;

public class zad4_5 {
        public static void main(String[] args){
            Osoba o1 = new Osoba("Kowalski", 2000);
            Student o2 = new Student("Nowak", 1998, "IT");
            Nauczyciel o3 = new Nauczyciel("Wilk", 1970, 4000);
            System.out.println(o1.toString());
            System.out.println(o2.toString());
            System.out.println(o3.toString());
            System.out.println(o1.getNazwisko());
            System.out.println(o3.getPensja());
        }
}
