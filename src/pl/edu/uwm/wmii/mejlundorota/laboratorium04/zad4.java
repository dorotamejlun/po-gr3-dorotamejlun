package pl.edu.uwm.wmii.mejlundorota.laboratorium04;

import java.math.BigInteger;
import java.util.Scanner;

public class zad4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Podaj n x n: ");
        int n = in.nextInt();
        int count = 0;
        int tmp = 1;
        int sum = 0;
        while (count != n*n){
            sum += tmp;
            tmp = tmp*2;
            count += 1;
        }
        System.out.println(sum);
    }
}