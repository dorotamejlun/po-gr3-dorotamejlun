package pl.edu.uwm.wmii.mejlundorota.laboratorium04;

import java.util.Arrays;
import java.util.Scanner;

public class zad1f {
    public static String change(String str){
        StringBuffer msg = new StringBuffer("");
        for (int i = 0; i < str.length(); i++){
            char c = str.charAt(i);
            if(Character.isUpperCase(c)){
                msg.append(Character.toLowerCase(c));
            } else {
                msg.append(Character.toUpperCase(c));
            }
        }
        return msg.toString();
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Input string ");
        String napis = in.nextLine();

        System.out.println(napis + ": " + change(napis));
    }
}
