package pl.edu.uwm.wmii.mejlundorota.laboratorium04;

import java.util.Arrays;
import java.util.Scanner;

public class zad1g {
    public static String nice(String str){
        StringBuffer msg = new StringBuffer("");
        int count = 0;
        for (int i = str.length()-1; i >= 0; i--){
            if(count == 3){
                msg.append('\'');
                count = 0;
            }
            msg.append(str.charAt(i));
            count++;
        }
        msg = msg.reverse();
        return msg.toString();
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Input string: ");
        String string = in.nextLine();
        System.out.println(string + ": " + nice(string));
    }
}