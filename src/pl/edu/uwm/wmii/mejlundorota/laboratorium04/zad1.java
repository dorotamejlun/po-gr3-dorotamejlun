package pl.edu.uwm.wmii.mejlundorota.laboratorium04;

import java.util.Scanner;

public class zad1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("String: ");
        String str = scanner.nextLine();
        System.out.println("Char: ");
        char c = scanner.next().charAt(0);

        System.out.println("'"+c+"' in '"+str+"' : "+countChar(str,c));
        System.out.println(countSubStr("ala ma kota ala ala", "ala"));

//        System.out.println("String: ");
//        String s = scanner.nextLine();
        System.out.println("The middle element: "+middle("koot"));

        Scanner new_string = new Scanner(System.in);
        System.out.println("Input string: ");
        String string = new_string.nextLine();
        Scanner n = new Scanner(System.in);
        System.out.println("Input num:  ");
        int n_ = n.nextInt();

        System.out.println("'"+string+"'"+"repeats"+n_+"times: "+repeat(string,n_));

    }
    public static int countChar(String str, char c){
        int count = 0;
        for(int i=0; i<str.length(); i++){
            if(str.charAt(i) == c){
                count++;
            }
        }
        return count;
    }
    public static int countSubStr(String str, String subStr){
        int count = 0;
        int index = 0;
        while (true){
            index = str.indexOf(subStr, index);
            if (index != -1) {
                count ++;
                index += subStr.length();
            } else {
                break;
            }
        }
        return count;
    }
    public static String middle(String str){
        int length = str.length();
        if(length % 2 == 0){
            return str.charAt(str.length() / 2 - 1) + Character.toString(str.charAt(str.length()/2));
        }
        else {
            return Character.toString(str.charAt(str.length()/2));
        }
    }
    public static String repeat(String str, int n){
        StringBuilder str1 = new StringBuilder("");
        for (int i=0;i<n;i++){
            str1.append(str);
        }
        return str1.toString();
    }
}