package pl.edu.uwm.wmii.mejlundorota.laboratorium12;

import java.util.Collections;
import java.util.LinkedList;

public class zad3 {
    public static void main(String[] args) {
        LinkedList<String> p1 = new LinkedList<String>();
        String[] l = {"a", "b", "c", "d", "e", "f", "g", "h"};
        for (String e : l) {
            p1.add(e);
        }
        System.out.println(p1);
        odwroc(p1);
        System.out.println(p1);
    }

    public static <T> void odwroc(LinkedList<T> pracownicy) {
        Collections.reverse(pracownicy);
    }
}
