package pl.edu.uwm.wmii.mejlundorota.laboratorium12;

import java.awt.*;
import java.util.LinkedList;

public class zad1 {
    public static void main(String[] args) {
        LinkedList<String> p1 = new LinkedList<String>();
        String[] l = {"a", "b", "c", "d", "e", "f", "g", "h"};
        for (String e : l) {
            p1.add(e);
        }
        System.out.println(p1);
        redukuj(p1, 1);
        System.out.println(p1);
//        odwroc(p1);
    }

    public static <T> void redukuj(LinkedList<T> pracownicy, int n) {
        int ile = 0;
        for (int i = 0; i < pracownicy.size(); i++) {
            if (ile == n-1) {
                pracownicy.remove(i);
                continue;
            }
            ile += 1;
        }
    }
}