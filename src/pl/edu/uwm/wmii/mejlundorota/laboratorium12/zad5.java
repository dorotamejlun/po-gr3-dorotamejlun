package pl.edu.uwm.wmii.mejlundorota.laboratorium12;

import java.util.Stack;
import java.util.regex.Pattern;

public class zad5 {
    public static void main(String[] args) {
        String s;
        s = "Ala ma kota.";
        System.out.println(reverse(s));
    }
    private static String reverse(String s){
        String [] arrString = s.trim().split(Pattern.quote(" "));
        Stack<String> stack = new Stack<String>();
        for(String e : arrString){
            if(e.endsWith(".")){
                stack.push(e.toLowerCase().substring(0, e.length() - 1));
                break;
            }else{
                stack.push(e);
            }
        }
        StringBuilder builder = new StringBuilder();
        while( !stack.isEmpty()) {
            if(stack.size() == 1){
                String element = stack.pop().toString();
                builder.append(element.substring(0, 1).toLowerCase() +
                        element.substring(1)).append(" ");
            }else{
                builder.append(stack.pop()).append(" ");
            }
        }
        builder.append(".");
        String result = builder.toString();
        return result.substring(0, 1).toUpperCase() + result.substring(1);
    }
}
