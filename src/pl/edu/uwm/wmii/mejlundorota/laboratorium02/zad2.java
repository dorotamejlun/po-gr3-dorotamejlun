package pl.edu.uwm.wmii.mejlundorota.laboratorium02;

import java.util.Scanner;
import java.util.Random;

public class zad2 {
    public static void main(String[] args) {
        System.out.println("Podaj liczbę n: ");
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        if (n < 1 || n > 100) {
            System.err.println("Błędna liczba!");
            System.exit(1);
        }
        int[] tab = new int[n];

        generuj(tab, -999, 999);
        System.out.println("Nieparzyste: "+ileNieparzystych(tab));
        System.out.println("Pparzyste: "+ileParzystych(tab));
        System.out.println("Dodatnie: "+ileDodatnich(tab));
        System.out.println("Ujemne: "+ileUjemnych(tab));
        System.out.println("Zero: "+ileZerowych(tab));
        System.out.println("Maksymalne: "+ileMaksymalnych(tab));
        System.out.println("Suma dodatnich: "+sumaDodatnich(tab));
        System.out.println("Suma ujemnych: "+sumaUjemnych(tab));
        System.out.println("Dł. maks. ciągu dodatnich: "+dlugoscMaksymalnegoCiaguDodatnich(tab));
        signum(tab);

        System.out.println("Lewy: ");
        int lewy = scanner.nextInt();
        if (1>lewy || n<lewy){
            System.err.println("Błędna liczba");
            System.exit(1);
        }
        System.out.println("Prawy: ");
        int prawy = scanner.nextInt();
        if (1>prawy || n<prawy){
            System.err.println("Błędna liczba");
            System.exit(1);
        }

        odwrocFragment(tab,lewy,prawy);

    }
    public static void generuj (int[] tab, int min, int max) {
        Random r = new Random();
        for (int j = 0; j < tab.length; ++j) {
            tab[j] = r.nextInt(max + 1 - min) + min;
        }
    }
    public static int ileNieparzystych(int[] tab){
        int ile = 0;
        for (int i=0;i<tab.length;i++) {
            if(tab[i]%2 != 0){
                ile++;
            }
        }
        return ile;
    }
    public static int ileParzystych(int[] tab){
        int ile = 0;
        for (int i=0;i<tab.length;i++) {
            if(tab[i]%2 == 0){
                ile++;
            }
        }
        return ile;
    }
    public static int ileDodatnich(int[] tab){
        int ile=0;
        for (int i=0;i<tab.length;i++) {
            if(tab[i]>0){
                ile++;
            }
        }
        return ile;
    }
    public static int ileUjemnych(int[] tab){
        int ile = 0;
        for (int i=0;i<tab.length;i++) {
            if(tab[i]<0){
                ile++;
            }
        }
        return ile;
    }
    public static int ileZerowych(int[] tab){
        int ile = 0;
        for (int i=0;i<tab.length;i++) {
            if(tab[i] == 0){
                ile++;
            }
        }
        return ile;
    }
    public static int ileMaksymalnych(int[] tab){
        int najwiekszy = tab[0];
        int ile = 1;

        for(int i=0; i<tab.length; i++){
            System.out.println(tab[i]);
            if(tab[i] == najwiekszy){
                ile++;
            } else if(tab[i] > najwiekszy){
                ile = 1;
                najwiekszy = tab[i];
            }
        }
        return ile;
    }
    public static int sumaDodatnich(int[] tab){
        int ile = 0;
        for (int i=1; i<tab.length; i++) {
            if(tab[i] > 0){
                ile += tab[i];
            }
        }
        return ile;
    }
    public static int sumaUjemnych(int[] tab){
        int ile = 0;
        for (int i=1; i<tab.length; i++) {
            if(tab[i] < 0){
                ile += tab[i];
            }
        }
        return ile;
    }
    public static int dlugoscMaksymalnegoCiaguDodatnich(int[] tab){
        int dl = 0;
        int max_dl = 0;
        for (int i=0;i<tab.length;i++){
            if(tab[i]>0){
                dl++;
            }
            else{
                if (dl > max_dl){
                    max_dl = dl;
                }
                dl = 0;
            }
        }
        return max_dl;
    }

    public static void signum(int[] tab){
        for (int i=0;i< tab.length;i++){
            if(tab[i]<0){
                tab[i] = -1;
            }
            else if(tab[i]>0){
                tab[i] = 1;
            }
            System.out.println(tab[i]);
        }
    }
    public static void odwrocFragment(int[] tab, int lewa, int prawa){
        for (int j=0;j<lewa;j++) {
            System.out.println(tab[j]);
        }
        for (int i=prawa;i>=lewa;i--){
            System.out.println(tab[i]);
        }
        for (int k=prawa+1;k<tab.length;k++){
            System.out.println(tab[k]);
        }
    }
}
