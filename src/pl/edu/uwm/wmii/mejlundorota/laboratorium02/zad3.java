package pl.edu.uwm.wmii.mejlundorota.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad3 {
    public static void main(String[] args){
        System.out.println("Podaj m: ");
        Scanner scanner = new Scanner(System.in);

        int m = scanner.nextInt();
        if (m<1 || m>10){
            System.err.println("Błędna liczba");
            System.exit(1);
        }
        System.out.println("Podaj n: ");
        int n = scanner.nextInt();
        if (n<1 || n>10){
            System.err.println("Błędna liczba");
            System.exit(1);
        }
        System.out.println("Podaj k: ");
        int k = scanner.nextInt();
        if (k<1 || k>10){
            System.err.println("Błędna liczba");
            System.exit(1);
        }
        int [][] a = new int [m][n];
        int [][] b = new int [n][k];

        System.out.println("Macierz a ("+m+"x"+n+"):");
        generuj (a,m,n,0,5);
        System.out.println("Macierz b("+n+"x"+k+"):");
        generuj (b,n,k,0,5);

        int [][] c = new int[m][k];

        for (int i=0;i<m;i++){

            for (int j=0;j<k;j++){
                c[i][j] = 0;
                for (int t=0;t<k;t++){
                    c[i][j] = c[i][j] + a[i][t] * b[t][j];
                }
                System.out.print(c[i][j]+"  ");
            }
            System.out.println("");
        }

    }
    public static void generuj (int[][] tab,int m, int n, int min, int max) {
        Random r = new Random();
        for (int j=0; j<m; j++) {
            for (int t=0;t<n;t++){
                tab[j][t] = r.nextInt(max + 1 - min) + min;
                System.out.print(tab[j][t]+"  ");
            }
            System.out.println("");
        }
        System.out.println("");
    }

}

