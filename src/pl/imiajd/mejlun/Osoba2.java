package pl.imiajd.mejlun;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public abstract class Osoba2 {
    public Osoba2(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec) {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }

    public abstract String getOpis();

    public String getNazwisko() {
        return nazwisko;
    }
    public String[] getImiona(){
        return imiona;
    }
    public String getFormattedImiona() {
        StringBuilder tmp = new StringBuilder("");
        for (String i : this.imiona){
            tmp.append(i).append(" ");
        }
        return tmp.toString();
    }
    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }
    public String getFormattedDate(LocalDate localdate){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
        return localdate.format(formatter);
    }

    public boolean getPlec()
    {
        if(this.plec){
            return true;
        }
        else {
            return false;
        }
    }
    public String get_Plec(){
        if (this.getPlec()){
            return "Kobieta";
        }
        else{
            return "Mężczyzna";
        }
    }

    public String nazwisko;
    public String[] imiona;
    public LocalDate dataUrodzenia;
    public boolean plec;
}
