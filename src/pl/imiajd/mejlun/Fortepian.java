package pl.imiajd.mejlun;

public class Fortepian extends Instrument{
    public Fortepian(String producent){
        super(producent);
    }
    public String dzwiek(){
        return "dzwięk fortepianu";
    }
}
