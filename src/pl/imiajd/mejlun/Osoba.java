package pl.imiajd.mejlun;


public class Osoba{

    public Osoba(String nazwisko, int rokUrodzenia){
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
    }
    public String getNazwisko(){
        return this.nazwisko;
    }
    public int getRokUrodzenia(){
        return this.rokUrodzenia;
    }
    public String toString(){
        return this.nazwisko +" "+this.rokUrodzenia;
    }

    protected String nazwisko;
    protected int rokUrodzenia;
}
