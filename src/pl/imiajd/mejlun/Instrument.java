package pl.imiajd.mejlun;

import java.time.LocalDate;

public abstract class Instrument {
    public Instrument(String producent){
        this.producent = producent;
//        this.rokProdukcji = rokProdukcji;
    }
    public abstract String dzwiek();

    public String getProducent(){
        return producent;
    }
    public boolean equals(Object otherObject){
        if(this == otherObject){
            return true;
        }
        if(otherObject == null){
            return false;
        }
        if(getClass() != otherObject.getClass()){
            return false;
        }
        Instrument other = (Instrument) otherObject;
        return other.getProducent().equals(this.getProducent());
    }

    public String toString(){
        return "Producent: "+producent+" Rok produkcji: ";
    }
    public String producent;
//    public LocalDate rokProdukcji;
}
