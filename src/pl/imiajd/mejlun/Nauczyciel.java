package pl.imiajd.mejlun;

public class Nauczyciel extends Osoba{

    public Nauczyciel(String nazwisko, int rokUrodzenia, int pensja){
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }
    public Integer getPensja(){
        return this.pensja;
    }
    public String toString(){
        return super.toString()+" "+this.pensja;
    }
    private int pensja;
}
