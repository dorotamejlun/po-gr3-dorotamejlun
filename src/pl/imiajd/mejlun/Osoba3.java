package pl.imiajd.mejlun;
import java.util.Objects;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Iterator;

interface IComCLon extends Comparable, Cloneable{}

public class Osoba3 implements IComCLon{
    public Osoba3(String nazwisko, int year, int month, int day){
        this.nazwisko = nazwisko;
        this.dataUrodzenia = LocalDate.of(year,month - 1,day);
    }
    public String getNazwisko(){
        return nazwisko;
    }
    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }
    public String toString() {
        return "Osoba3 [" + nazwisko + "]"+ " dataUrodzenia " + dataUrodzenia.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    public int compareTo(Object otherObject) {
        Osoba3 other = (Osoba3)otherObject;
        int i = this.nazwisko.compareTo(other.nazwisko);
        return i != 0 ? i : dataUrodzenia.compareTo(other.dataUrodzenia);
    }

    public boolean equals(Object object) {
        if (this == object)
            return true;
        if (object == null || getClass() != object.getClass())
            return false;
        Osoba3 osoba3 = (Osoba3) object;
        return Objects.equals(nazwisko, osoba3.nazwisko) && Objects.equals(dataUrodzenia, osoba3.dataUrodzenia);
    }
    public int hashCode() {
        return Objects.hash(nazwisko, dataUrodzenia);
    }
    private String nazwisko;
    private LocalDate dataUrodzenia;
}

