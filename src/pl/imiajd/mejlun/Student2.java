package pl.imiajd.mejlun;

import java.time.LocalDate;

public class Student2 extends Osoba2 {
    public Student2(String nazwisko, String[] imiona, LocalDate dataUrodzenia, String kierunek, double sredniaOcen, boolean plec) {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getKierunek(){
        return kierunek;
    }

    public String getOpis() {
        return String.format(" Kierunek studiów: " + kierunek + ", Srednia ocen: "+ sredniaOcen);
    }

    public String kierunek;
    public double sredniaOcen;
    public LocalDate dataUrodzenia;
}

