package pl.imiajd.mejlun;

public class Adres {
    public Adres(String ulica, int numer_domu, String miasto, String kod_pocztowy) {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public Adres(String ulica, int numer_domu, String miasto, String kod_pocztowy, int numer_mieszkania) {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
        this.numer_mieszkania = numer_mieszkania;
    }

    public void pokaz() {
        if (this.numer_mieszkania == 0) {
            System.out.println(kod_pocztowy + " " + miasto);
            System.out.println(numer_domu);
        } else {
            System.out.println(kod_pocztowy + " " + miasto);
            System.out.println(numer_domu + " m" + numer_mieszkania);
        }
    }

    public String ulica;
    public int numer_domu;
    public String miasto;
    public String kod_pocztowy;
    public int numer_mieszkania;
}
