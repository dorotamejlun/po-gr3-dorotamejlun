package pl.imiajd.mejlun;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Pracownik2 extends Osoba2 {
    public Pracownik2(String nazwisko, String[] imiona, LocalDate dataUrodzenia, double pobory, LocalDate dataZatrudnienia, boolean plec) {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory() {
        return pobory;
    }
    public LocalDate getDataZatrudnienia(){
        return dataZatrudnienia;
    }

    public String getFormattedDate(LocalDate localdate){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
        return localdate.format(formatter);
    }

    public String getOpis() {
        return String.format(" Pracownik z pensją %.2f zł  Data zatrudnienia %s: ", pobory, getFormattedDate(dataZatrudnienia));
    }

    protected double pobory;
    protected LocalDate dataZatrudnienia;
}


