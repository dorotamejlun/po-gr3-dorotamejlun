package pl.imiajd.mejlun;

import java.time.LocalDate;
import java.util.Objects;

public class Student3 extends Osoba3 implements IComCLon {
    public Student3(String name, int year, int month, int day, double sredniaOcen) {
        super(name, year, month, day);
        this.sredniaOcen = sredniaOcen;
    }

    public int compareTo(Object otherObject) {
        int parentCompare = super.compareTo(otherObject);
        if (parentCompare == 0) {
            Student3 other = (Student3) otherObject;
            return Double.compare(this.sredniaOcen, other.sredniaOcen);
        } else {
            return parentCompare;
        }
    }

    public String toString() {
        double var = this.sredniaOcen;
        return "Student3 [sredniaOcen =" + var + "] " + super.toString();
    }

    private double sredniaOcen;
}
